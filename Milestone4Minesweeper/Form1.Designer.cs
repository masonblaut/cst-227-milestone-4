﻿using MinesweeperClasses;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Milestone4Minesweeper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        static public Board myBoard;
        public Button[,] btnGrid;

        static int gameGoal = 0;
        static int gameProgress = 0;

        public void populateGrid( int boardSize)
        {
            myBoard = new Board(boardSize);
            btnGrid = new Button[myBoard.Size, myBoard.Size];
            int buttonSize = panel1.Width / myBoard.Size;
            panel1.Height = panel1.Width;
            myBoard.assignBombs();

            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {
                    btnGrid[r, c] = new Button();

                    btnGrid[r, c].Width = buttonSize;
                    btnGrid[r, c].Height = buttonSize;
                    btnGrid[r, c].Tag = r.ToString() + "," + c.ToString();

                    btnGrid[r, c].Click += Grid_Button_Click;
                    panel1.Controls.Add(btnGrid[r, c]);
                    btnGrid[r, c].Location = new Point(buttonSize * r, buttonSize * c);
                    
                }
            }
        }

        private void Grid_Button_Click(object sender, EventArgs e)
        {
            Button clickedButton = (Button)sender;

            string[] strArr = (sender as Button).Tag.ToString().Split(',');
            int r = int.Parse(strArr[0]);
            int c = int.Parse(strArr[1]);

            myBoard.theGrid[r, c].isVisited = true;
            updateButtonLabels();

            if (gameProgress == gameGoal - 1)
            {
                button1.Text = "You Win!!!";
            }


        }


        public void checkGrid()
        {
            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {

                    if (myBoard.theGrid[r, c].hasBomb != true)
                    {
                        gameGoal++;
                    }


                }
            }
        }


        public void updateButtonLabels()
        {
            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {
                    if (myBoard.theGrid[r, c].isVisited)
                    {
                        if (myBoard.theGrid[r, c].hasBomb != true && myBoard.theGrid[r, c].nextCount >= 1)
                        {
                            btnGrid[r, c].Text = "" + myBoard.theGrid[r, c].nextCount;
                            gameProgress++;
                        }
                        if (myBoard.theGrid[r, c].hasBomb != true && myBoard.theGrid[r, c].nextCount < 1)
                        {
                            btnGrid[r, c].Text = "~";
                            gameProgress++;
                        }
                        if (myBoard.theGrid[r, c].hasBomb)
                        {
                            btnGrid[r, c].Text = "Live Bomb";
                            button1.Text = "Game Over!!!";
                        }
                    }


                }
            }

        }



        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(12, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(627, 634);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(22, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(199, 35);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 699);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Button button1;
    }
}

