﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinesweeperClasses
{
    public class MinesweeperGameDriver : IPlayable
    {
        static public int boardSize = 8;
        static public Board myBoard = new Board(boardSize);
        static public bool gameOver = false;

        static int gameGoal = boardSize*boardSize;
        static int gameProgress = 0;

        public void playGame()
        {
            myBoard.assignBombs();
            setGameGoal();


            while (gameOver != true)
            {
                Console.Clear();
                revealBoard();
                Console.WriteLine("Game Goal: "+gameGoal+" Game Progress: "+gameProgress);
                gameWin();
                promptVisit();
            }

        }

        public void revealBoard()
        {
            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {
                    if (myBoard.theGrid[r, c].hasBomb && myBoard.theGrid[r,c].isVisited)
                    {
                        Console.Write(" x ");
                    }
                    else if (myBoard.theGrid[r, c].nextToBomb && myBoard.theGrid[r,c].isVisited)
                    {
                        Console.Write(" " + myBoard.theGrid[r, c].nextCount + " ");
                    }
                    else if (myBoard.theGrid[r, c].isVisited)
                    {
                        Console.Write(" ~ ");
                    }
                    else
                    {
                        Console.Write(" ? ");
                    }
                }
                Console.WriteLine();
            }
        }

        public void promptVisit()
        {
            int row = 0;
            int col = 0;
            Console.Write("Visit a row: ");
            row = int.Parse(Console.ReadLine());
            Console.Write("Visit a column: ");
            col = int.Parse(Console.ReadLine());
            visitSquare(row, col);
        }

        public void visitSquare(int row, int col)
        {
            

            if(myBoard.theGrid[row, col].hasBomb)
            {
                gameLose();
            }
            else
            {
                noBombFlood(row, col);
            }
        }

        public void gameLose()
        {
            Console.WriteLine("BOOM!!! Game Over");
            revealAllBoard();
            gameOver = true;
        }

        public void revealAllBoard()
        {
            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {
                    if (myBoard.theGrid[r, c].hasBomb)
                    {
                        Console.Write(" x ");
                    }
                    else if (myBoard.theGrid[r, c].nextToBomb)
                    {
                        Console.Write(" " + myBoard.theGrid[r, c].nextCount + " ");
                    }
                    else
                    {
                        Console.Write(" ~ ");
                    }
                }
                Console.WriteLine();
            }
        }

        public void setGameGoal()
        {
            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {
                    if (myBoard.theGrid[r, c].hasBomb)
                    {
                        gameGoal--;
                    }
                }
            }
        }

        public void gameWin()
        {
            if (gameGoal == gameProgress)
            {
                Console.WriteLine("Game Over!!! You Win!");
            }
        }

        public void noBombFlood(int x, int y)
        {
            if (x < 0 || x > boardSize-1 || y < 0 || y > boardSize-1 ||
                myBoard.theGrid[x, y].isVisited == true)
            {
                return;
            }

            gameProgress++;
            myBoard.theGrid[x, y].visited();

            if (myBoard.theGrid[x, y].nextCount == 0)
            {
                noBombFlood(x - 1, y);
                noBombFlood(x, y - 1);
                noBombFlood(x, y + 1);
                noBombFlood(x + 1, y);
            }
            
        }
    }
}
